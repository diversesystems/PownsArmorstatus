package me.powns.armorhud.settings.modules;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import net.minecraft.client.Minecraft;
import java.util.ArrayList;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import java.awt.Color;

public class HelmetSettings
{
    private int minX;
    private int minY;
    private int maxX;
    private int maxY;
    private int addX;
    private int addY;
    private int duraColorR1;
    private int duraColorG1;
    private int duraColorB1;
    private int color1;
    private int duraColorR2;
    private int duraColorG2;
    private int duraColorB2;
    private int color2;
    private int duraColorR3;
    private int duraColorG3;
    private int duraColorB3;
    private int color3;
    private int duraColorR4;
    private int duraColorG4;
    private int duraColorB4;
    private int color4;
    private boolean chroma;
    private boolean shown;
    
    public HelmetSettings() {
        this.addX = 0;
        this.addY = 0;
        this.duraColorR1 = 0;
        this.duraColorG1 = 160;
        this.duraColorB1 = 0;
        this.duraColorR2 = 220;
        this.duraColorG2 = 210;
        this.duraColorB2 = 0;
        this.duraColorR3 = 190;
        this.duraColorG3 = 120;
        this.duraColorB3 = 0;
        this.duraColorR4 = 160;
        this.duraColorG4 = 0;
        this.duraColorB4 = 0;
        float[] theColor = Color.RGBtoHSB(this.duraColorR1, this.duraColorG1, this.duraColorB1, null);
        this.color1 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR2, this.duraColorG2, this.duraColorB2, null);
        this.color2 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR3, this.duraColorG3, this.duraColorB3, null);
        this.color3 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR4, this.duraColorG4, this.duraColorB4, null);
        this.color4 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        this.chroma = false;
        this.shown = true;
    }
    
    @SubscribeEvent
    public void onRenderTick(final TickEvent.RenderTickEvent e) {
        final int minX = this.addX;
        final int minY = this.addY;
        final int maxX = this.addX + 30;
        final int maxY = this.addY + 15;
        float[] theColor = Color.RGBtoHSB(this.duraColorR1, this.duraColorG1, this.duraColorB1, null);
        this.color1 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR2, this.duraColorG2, this.duraColorB2, null);
        this.color2 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR3, this.duraColorG3, this.duraColorB3, null);
        this.color3 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        theColor = Color.RGBtoHSB(this.duraColorR4, this.duraColorG4, this.duraColorB4, null);
        this.color4 = Color.HSBtoRGB(theColor[0], theColor[1], theColor[2]);
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
    }
    
    public void setDuraColor(final int colorCat, final int r, final int g, final int b) {
        switch (colorCat) {
            case 1: {
                this.duraColorR1 = r;
                this.duraColorG1 = g;
                this.duraColorB1 = b;
                break;
            }
            case 2: {
                this.duraColorR2 = r;
                this.duraColorG2 = g;
                this.duraColorB2 = b;
                break;
            }
            case 3: {
                this.duraColorR3 = r;
                this.duraColorG3 = g;
                this.duraColorB3 = b;
                break;
            }
            case 4: {
                this.duraColorR4 = r;
                this.duraColorG4 = g;
                this.duraColorB4 = b;
                break;
            }
        }
    }
    
    public ArrayList<Integer> getDuraRGBValues(final int colorCat) {
        final ArrayList<Integer> result = new ArrayList<Integer>();
        switch (colorCat) {
            case 1: {
                result.add(this.duraColorR1);
                result.add(this.duraColorG1);
                result.add(this.duraColorB1);
                break;
            }
            case 2: {
                result.add(this.duraColorR2);
                result.add(this.duraColorG2);
                result.add(this.duraColorB2);
                break;
            }
            case 3: {
                result.add(this.duraColorR3);
                result.add(this.duraColorG3);
                result.add(this.duraColorB3);
                break;
            }
            case 4: {
                result.add(this.duraColorR4);
                result.add(this.duraColorG4);
                result.add(this.duraColorB4);
                break;
            }
        }
        return result;
    }
    
    public int getDuraColor(final int colorCat) {
        if (this.chroma) {
            return Color.HSBtoRGB((float)(System.currentTimeMillis() % 1000L) / 1000.0f, 0.8f, 0.8f);
        }
        switch (colorCat) {
            case 1: {
                return this.color1;
            }
            case 2: {
                return this.color2;
            }
            case 3: {
                return this.color3;
            }
            case 4: {
                return this.color4;
            }
            default: {
                return -1;
            }
        }
    }
    
    public void saveConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "Powns' ArmorStatus HUD", "helmet.cfg");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final FileWriter writer = new FileWriter(file, false);
            writer.write("Enabled?:\n" + this.shown + "\n \nPositioning:\n" + this.addX + "\n" + this.addY + "\n \nChroma?:\n" + this.chroma + "\n \nColor1:\n" + this.duraColorR1 + "\n" + this.duraColorG1 + "\n" + this.duraColorB1 + "\n \nColor2:\n" + this.duraColorR2 + "\n" + this.duraColorG2 + "\n" + this.duraColorB2 + "\n \nColor3:\n" + this.duraColorR3 + "\n" + this.duraColorG3 + "\n" + this.duraColorB3 + "\n \nColor4:\n" + this.duraColorR4 + "\n" + this.duraColorG4 + "\n" + this.duraColorB4 + "\n \n");
            writer.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public void loadConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "Powns' ArmorStatus HUD", "helmet.cfg");
            if (!file.exists()) {
                return;
            }
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            int i = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                switch (++i) {
                    case 2: {
                        this.shown = Boolean.parseBoolean(line);
                        continue;
                    }
                    case 5: {
                        this.addX = Integer.parseInt(line);
                        continue;
                    }
                    case 6: {
                        this.addY = Integer.parseInt(line);
                        continue;
                    }
                    case 9: {
                        this.chroma = Boolean.parseBoolean(line);
                        continue;
                    }
                    case 12: {
                        this.duraColorR1 = Integer.parseInt(line);
                        continue;
                    }
                    case 13: {
                        this.duraColorG1 = Integer.parseInt(line);
                        continue;
                    }
                    case 14: {
                        this.duraColorB1 = Integer.parseInt(line);
                        continue;
                    }
                    case 17: {
                        this.duraColorR2 = Integer.parseInt(line);
                        continue;
                    }
                    case 18: {
                        this.duraColorG2 = Integer.parseInt(line);
                        continue;
                    }
                    case 19: {
                        this.duraColorB2 = Integer.parseInt(line);
                        continue;
                    }
                    case 22: {
                        this.duraColorR3 = Integer.parseInt(line);
                        continue;
                    }
                    case 23: {
                        this.duraColorG3 = Integer.parseInt(line);
                        continue;
                    }
                    case 24: {
                        this.duraColorB3 = Integer.parseInt(line);
                        continue;
                    }
                    case 27: {
                        this.duraColorR4 = Integer.parseInt(line);
                        continue;
                    }
                    case 28: {
                        this.duraColorG4 = Integer.parseInt(line);
                        continue;
                    }
                    case 29: {
                        this.duraColorB4 = Integer.parseInt(line);
                        continue;
                    }
                    default: {
                        continue;
                    }
                }
            }
            reader.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public int getMinX() {
        return this.minX;
    }
    
    public int getMinY() {
        return this.minY;
    }
    
    public int getMaxX() {
        return this.maxX;
    }
    
    public int getMaxY() {
        return this.maxY;
    }
    
    public int getAddX() {
        return this.addX;
    }
    
    public void setAddX(final int addX) {
        this.addX = addX;
    }
    
    public int getAddY() {
        return this.addY;
    }
    
    public void setAddY(final int addY) {
        this.addY = addY;
    }
    
    public boolean isChroma() {
        return this.chroma;
    }
    
    public void setChroma(final boolean chroma) {
        this.chroma = chroma;
    }
    
    public boolean isShown() {
        return this.shown;
    }
    
    public void setShown(final boolean shown) {
        this.shown = shown;
    }
}
