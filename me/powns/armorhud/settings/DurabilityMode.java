package me.powns.armorhud.settings;

public enum DurabilityMode
{
    NONE("None"), 
    REGULAR("Default"), 
    PERCENTAGE("Percentage"), 
    COMPARED("Compared");
    
    private String name;
    
    private DurabilityMode(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}
