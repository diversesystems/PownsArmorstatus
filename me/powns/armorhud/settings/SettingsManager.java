package me.powns.armorhud.settings;

import me.powns.armorhud.settings.modules.HeldItemSettings;
import me.powns.armorhud.settings.modules.BootsSettings;
import me.powns.armorhud.settings.modules.LeggingsSettings;
import me.powns.armorhud.settings.modules.ChestplateSettings;
import me.powns.armorhud.settings.modules.HelmetSettings;

public class SettingsManager
{
    private GlobalSettings global;
    private HelmetSettings helmet;
    private ChestplateSettings chestplate;
    private LeggingsSettings leggings;
    private BootsSettings boots;
    private HeldItemSettings helditem;
    
    public SettingsManager() {
        this.global = new GlobalSettings();
        this.helmet = new HelmetSettings();
        this.chestplate = new ChestplateSettings();
        this.leggings = new LeggingsSettings();
        this.boots = new BootsSettings();
        this.helditem = new HeldItemSettings();
    }
    
    public void saveSettings() {
        this.global.saveConfig();
        this.helmet.saveConfig();
        this.chestplate.saveConfig();
        this.leggings.saveConfig();
        this.boots.saveConfig();
        this.helditem.saveConfig();
    }
    
    public void loadSettings() {
        this.helmet.loadConfig();
        this.chestplate.loadConfig();
        this.leggings.loadConfig();
        this.boots.loadConfig();
        this.helditem.loadConfig();
    }
    
    public GlobalSettings getGlobalSettings() {
        return this.global;
    }
    
    public HelmetSettings getHelmetSettings() {
        return this.helmet;
    }
    
    public ChestplateSettings getChestplateSettings() {
        return this.chestplate;
    }
    
    public LeggingsSettings getLeggingsSettings() {
        return this.leggings;
    }
    
    public BootsSettings getBootsSettings() {
        return this.boots;
    }
    
    public HeldItemSettings getHelditemSettings() {
        return this.helditem;
    }
}
