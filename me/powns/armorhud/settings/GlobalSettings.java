package me.powns.armorhud.settings;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import net.minecraft.client.Minecraft;

public class GlobalSettings
{
    private boolean renderOverlay;
    private int currentDuraMode;
    private boolean enabled;
    
    public GlobalSettings() {
        this.renderOverlay = true;
        this.enabled = true;
        this.currentDuraMode = 1;
    }
    
    public void saveConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "Powns' ArmorStatus HUD", "global.cfg");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final FileWriter writer = new FileWriter(file, false);
            writer.write("Enabled?: \n" + this.enabled + "\n \nShow Durability Bars?: \n" + this.renderOverlay + "\n \nDurability Mode: \n" + this.currentDuraMode);
            writer.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public void loadConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "Powns' ArmorStatus HUD", "global.cfg");
            if (!file.exists()) {
                return;
            }
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            int i = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                ++i;
            }
            reader.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public boolean isRenderOverlay() {
        return this.renderOverlay;
    }
    
    public void setRenderOverlay(final boolean renderOverlay) {
        this.renderOverlay = renderOverlay;
    }
    
    public int getCurrentDuraModeIndex() {
        return this.currentDuraMode;
    }
    
    public DurabilityMode getCurrentDuraMode() {
        return DurabilityMode.values()[this.currentDuraMode];
    }
    
    public void setCurrentDuraMode(final int currentDuraMode) {
        this.currentDuraMode = currentDuraMode;
    }
    
    public boolean isEnabled() {
        return this.enabled;
    }
    
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}
