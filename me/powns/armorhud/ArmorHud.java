package me.powns.armorhud;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import me.powns.armorhud.gui.menus.DuraColorMenuGui;
import me.powns.armorhud.gui.menus.MainMenuGui;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommand;
import me.powns.armorhud.command.ArmorHudCommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import me.powns.armorhud.gui.HudRenderer;
import me.powns.armorhud.settings.SettingsManager;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "Powns ArmorStatus HUD", version = "1.0")
public class ArmorHud
{
    public static final String MODID = "Powns ArmorStatus HUD";
    public static final String VERSION = "1.0";
    private Minecraft mc;
    private SettingsManager sm;
    private HudRenderer hr;
    private boolean mainMenuOpen;
    
    public ArmorHud() {
        this.mc = Minecraft.getMinecraft();
        this.sm = new SettingsManager();
        this.hr = new HudRenderer(this, this.sm);
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        ClientCommandHandler.instance.registerCommand((ICommand)new ArmorHudCommand(this));
        MinecraftForge.EVENT_BUS.register((Object)this);
        MinecraftForge.EVENT_BUS.register((Object)this.sm.getHelmetSettings());
        MinecraftForge.EVENT_BUS.register((Object)this.sm.getChestplateSettings());
        MinecraftForge.EVENT_BUS.register((Object)this.sm.getLeggingsSettings());
        MinecraftForge.EVENT_BUS.register((Object)this.sm.getBootsSettings());
        MinecraftForge.EVENT_BUS.register((Object)this.sm.getHelditemSettings());
        this.hr.centerVertical();
        this.sm.loadSettings();
    }
    
    @SubscribeEvent
    public void onRenderTick(final TickEvent.RenderTickEvent e) {
        if (this.mc.theWorld != null && this.sm.getGlobalSettings().isEnabled() && (this.mc.currentScreen == null || this.mc.currentScreen instanceof MainMenuGui || this.mc.currentScreen instanceof DuraColorMenuGui)) {
            this.hr.renderHud();
        }
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (this.mainMenuOpen) {
            this.mc.displayGuiScreen((GuiScreen)new MainMenuGui(this));
            this.mainMenuOpen = false;
        }
    }
    
    public void openMainMenu() {
        this.mainMenuOpen = true;
    }
    
    public SettingsManager getSm() {
        return this.sm;
    }
    
    public HudRenderer getHr() {
        return this.hr;
    }
}
