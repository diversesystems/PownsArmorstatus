package me.powns.armorhud.gui;

import me.powns.armorhud.settings.DurabilityMode;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import org.lwjgl.opengl.GL11;
import me.powns.armorhud.settings.modules.HeldItemSettings;
import me.powns.armorhud.settings.modules.BootsSettings;
import me.powns.armorhud.settings.modules.LeggingsSettings;
import me.powns.armorhud.settings.modules.ChestplateSettings;
import me.powns.armorhud.settings.modules.HelmetSettings;
import net.minecraft.item.ItemStack;
import me.powns.armorhud.settings.SettingsManager;
import net.minecraft.client.Minecraft;
import me.powns.armorhud.ArmorHud;

public class HudRenderer
{
    private ArmorHud mod;
    private Minecraft mc;
    private SettingsManager sm;
    
    public HudRenderer(final ArmorHud mod, final SettingsManager sm) {
        this.mod = mod;
        this.mc = Minecraft.getMinecraft();
        this.sm = sm;
    }
    
    public void renderHud() {
        if (this.sm.getHelmetSettings().isShown()) {
            this.renderHelmet();
        }
        if (this.sm.getChestplateSettings().isShown()) {
            this.renderChestplate();
        }
        if (this.sm.getLeggingsSettings().isShown()) {
            this.renderLeggings();
        }
        if (this.sm.getBootsSettings().isShown()) {
            this.renderBoots();
        }
        if (this.sm.getHelditemSettings().isShown()) {
            this.renderHeldItem();
        }
    }
    
    private void renderHelmet() {
        if (Minecraft.getMinecraft().thePlayer.inventory != null && Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(3) != null) {
            final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(3);
            final HelmetSettings settings = this.sm.getHelmetSettings();
            this.drawItem(is, settings.getMinX(), settings.getMinY());
            this.mc.fontRendererObj.drawStringWithShadow(this.getDuraString(is), (float)(settings.getMinX() + 16), (float)(settings.getMinY() + 5), settings.getDuraColor(this.getDuraStringColorCat(is)));
        }
    }
    
    private void renderChestplate() {
        if (Minecraft.getMinecraft().thePlayer.inventory != null && Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(2) != null) {
            final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(2);
            final ChestplateSettings settings = this.sm.getChestplateSettings();
            this.drawItem(is, settings.getMinX(), settings.getMinY());
            this.mc.fontRendererObj.drawStringWithShadow(this.getDuraString(is), (float)(settings.getMinX() + 16), (float)(settings.getMinY() + 5), settings.getDuraColor(this.getDuraStringColorCat(is)));
        }
    }
    
    private void renderLeggings() {
        if (Minecraft.getMinecraft().thePlayer.inventory != null && Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(1) != null) {
            final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(1);
            final LeggingsSettings settings = this.sm.getLeggingsSettings();
            this.drawItem(is, settings.getMinX(), settings.getMinY());
            this.mc.fontRendererObj.drawStringWithShadow(this.getDuraString(is), (float)(settings.getMinX() + 16), (float)(settings.getMinY() + 5), settings.getDuraColor(this.getDuraStringColorCat(is)));
        }
    }
    
    private void renderBoots() {
        if (Minecraft.getMinecraft().thePlayer.inventory != null && Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(0) != null) {
            final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.armorItemInSlot(0);
            final BootsSettings settings = this.sm.getBootsSettings();
            this.drawItem(is, settings.getMinX(), settings.getMinY());
            this.mc.fontRendererObj.drawStringWithShadow(this.getDuraString(is), (float)(settings.getMinX() + 16), (float)(settings.getMinY() + 5), settings.getDuraColor(this.getDuraStringColorCat(is)));
        }
    }
    
    private void renderHeldItem() {
        if (Minecraft.getMinecraft().thePlayer.inventory != null && Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem() != null) {
            final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem();
            final HeldItemSettings settings = this.sm.getHelditemSettings();
            this.drawItem(is, settings.getMinX(), settings.getMinY());
            this.mc.fontRendererObj.drawStringWithShadow(this.getDuraString(is), (float)(settings.getMinX() + 16), (float)(settings.getMinY() + 5), settings.getDuraColor(this.getDuraStringColorCat(is)));
        }
    }
    
    public void centerHorizontal() {
        final int helmetX = this.sm.getHelmetSettings().getMinX();
        final int helmetY = this.sm.getHelmetSettings().getMinY();
        final HelmetSettings helmet = this.sm.getHelmetSettings();
        final ChestplateSettings chestplate = this.sm.getChestplateSettings();
        final LeggingsSettings leggings = this.sm.getLeggingsSettings();
        final BootsSettings boots = this.sm.getBootsSettings();
        final HeldItemSettings helditem = this.sm.getHelditemSettings();
        switch (this.sm.getGlobalSettings().getCurrentDuraMode()) {
            case NONE: {
                chestplate.setAddX(helmetX + 15);
                chestplate.setAddY(helmetY);
                leggings.setAddX(helmetX + 30);
                leggings.setAddY(helmetY);
                boots.setAddX(helmetX + 45);
                boots.setAddY(helmetY);
                helditem.setAddX(helmetX + 63);
                helditem.setAddY(helmetY);
                break;
            }
            case REGULAR: {
                chestplate.setAddX(helmetX + 37);
                chestplate.setAddY(helmetY);
                leggings.setAddX(helmetX + 72);
                leggings.setAddY(helmetY);
                boots.setAddX(helmetX + 107);
                boots.setAddY(helmetY);
                helditem.setAddX(helmetX + 144);
                helditem.setAddY(helmetY);
                break;
            }
            case PERCENTAGE: {
                chestplate.setAddX(helmetX + 52);
                chestplate.setAddY(helmetY);
                leggings.setAddX(helmetX + 102);
                leggings.setAddY(helmetY);
                boots.setAddX(helmetX + 152);
                boots.setAddY(helmetY);
                helditem.setAddX(helmetX + 202);
                helditem.setAddY(helmetY);
                break;
            }
            case COMPARED: {
                chestplate.setAddX(helmetX + 62);
                chestplate.setAddY(helmetY);
                leggings.setAddX(helmetX + 122);
                leggings.setAddY(helmetY);
                boots.setAddX(helmetX + 182);
                boots.setAddY(helmetY);
                helditem.setAddX(helmetX + 242);
                helditem.setAddY(helmetY);
                break;
            }
        }
    }
    
    public void centerVertical() {
        final int helmetX = this.sm.getHelmetSettings().getMinX();
        final int helmetY = this.sm.getHelmetSettings().getMinY();
        final HelmetSettings helmet = this.sm.getHelmetSettings();
        final ChestplateSettings chestplate = this.sm.getChestplateSettings();
        final LeggingsSettings leggings = this.sm.getLeggingsSettings();
        final BootsSettings boots = this.sm.getBootsSettings();
        final HeldItemSettings helditem = this.sm.getHelditemSettings();
        chestplate.setAddX(helmetX);
        chestplate.setAddY(helmetY + 13);
        leggings.setAddX(helmetX);
        leggings.setAddY(helmetY + 27);
        boots.setAddX(helmetX);
        boots.setAddY(helmetY + 40);
        helditem.setAddX(helmetX);
        helditem.setAddY(helmetY + 56);
    }
    
    public void drawItem(final ItemStack is, final int x, final int y) {
        GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.translate(0.0f, 0.0f, 32.0f);
        final int count = is.stackSize;
        final String amountToDisplay = "" + ((count > 1) ? count : "");
        this.mc.getRenderItem().renderItemAndEffectIntoGUI(is, x, y);
        if (this.sm.getGlobalSettings().isRenderOverlay()) {
            this.mc.getRenderItem().renderItemOverlayIntoGUI(this.mc.fontRendererObj, is, x, y, amountToDisplay);
        }
        else {
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            GlStateManager.disableBlend();
            this.mc.fontRendererObj.drawStringWithShadow(amountToDisplay, (float)(x + 19 - 2 - this.mc.fontRendererObj.getStringWidth(amountToDisplay)), (float)(y + 6 + 3), 16777215);
            GlStateManager.enableLighting();
            GlStateManager.enableDepth();
        }
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableLighting();
        GL11.glPopMatrix();
    }
    
    private int getDuraStringColorCat(final ItemStack is) {
        final double max = (double)is.getMaxDamage();
        final double dmg = (double)is.getItemDamage();
        final double percentage = (max - dmg) / max * 100.0;
        if (percentage <= 100.0 && percentage > 75.0) {
            return 1;
        }
        if (percentage < 75.0 && percentage >= 50.0) {
            return 2;
        }
        if (percentage < 50.0 && percentage >= 25.0) {
            return 2;
        }
        if (percentage < 25.0 && percentage >= 0.0) {
            return 2;
        }
        return -1;
    }
    
    private String getDuraString(final ItemStack is) {
        String result = "";
        if (is.getMaxDamage() > 0) {
            switch (this.sm.getGlobalSettings().getCurrentDuraMode()) {
                case REGULAR: {
                    result = "" + (is.getMaxDamage() - is.getItemDamage());
                    break;
                }
                case PERCENTAGE: {
                    final NumberFormat formatter = new DecimalFormat("#0.00");
                    final double max = (double)is.getMaxDamage();
                    final double dmg = (double)is.getItemDamage();
                    final double percentage = (max - dmg) / max * 100.0;
                    result = ((percentage == 100.0) ? "100%" : (formatter.format(percentage) + "%"));
                    break;
                }
                case COMPARED: {
                    result = "" + (is.getMaxDamage() - is.getItemDamage()) + "/" + is.getMaxDamage();
                    break;
                }
            }
        }
        return result;
    }
}
