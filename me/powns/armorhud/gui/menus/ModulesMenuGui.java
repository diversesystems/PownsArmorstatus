package me.powns.armorhud.gui.menus;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import me.powns.armorhud.settings.GlobalSettings;
import me.powns.armorhud.settings.SettingsManager;
import me.powns.armorhud.ArmorHud;
import net.minecraft.client.gui.GuiScreen;

public class ModulesMenuGui extends GuiScreen
{
    private ArmorHud mod;
    private SettingsManager sm;
    private GlobalSettings global;
    private GuiButton helmet;
    private GuiButton chestplate;
    private GuiButton leggings;
    private GuiButton boots;
    private GuiButton held;
    private GuiButton goback;
    private GuiButton helmetT;
    private GuiButton chestplateT;
    private GuiButton leggingsT;
    private GuiButton bootsT;
    private GuiButton heldT;
    
    public ModulesMenuGui(final ArmorHud mod) {
        this.mod = mod;
        this.sm = this.mod.getSm();
        this.global = this.sm.getGlobalSettings();
    }
    
    public void initGui() {
        this.buttonList.add(this.helmet = new GuiButton(0, this.getCenter() - 30, this.getRowPos(2), 30, 10, "Edit"));
        this.buttonList.add(this.chestplate = new GuiButton(1, this.getCenter() - 30, this.getRowPos(3), 30, 10, "Edit"));
        this.buttonList.add(this.leggings = new GuiButton(2, this.getCenter() - 30, this.getRowPos(4), 30, 10, "Edit"));
        this.buttonList.add(this.boots = new GuiButton(3, this.getCenter() - 30, this.getRowPos(5), 30, 10, "Edit"));
        this.buttonList.add(this.held = new GuiButton(4, this.getCenter() - 30, this.getRowPos(6) + 8, 30, 10, "Edit"));
        this.buttonList.add(this.goback = new GuiButton(5, this.getCenter() - 35, this.getRowPos(8), 70, 20, "Return"));
        this.buttonList.add(this.helmetT = new GuiButton(6, this.getCenter(), this.getRowPos(2), 30, 10, this.getModuleEnabledState(1)));
        this.buttonList.add(this.chestplateT = new GuiButton(7, this.getCenter(), this.getRowPos(3), 30, 10, this.getModuleEnabledState(2)));
        this.buttonList.add(this.leggingsT = new GuiButton(8, this.getCenter(), this.getRowPos(4), 30, 10, this.getModuleEnabledState(3)));
        this.buttonList.add(this.bootsT = new GuiButton(9, this.getCenter(), this.getRowPos(5), 30, 10, this.getModuleEnabledState(4)));
        this.buttonList.add(this.heldT = new GuiButton(10, this.getCenter(), this.getRowPos(6) + 8, 30, 10, this.getModuleEnabledState(5)));
    }
    
    public int getRowPos(final int rowNumber) {
        return this.height / 4 + (24 * rowNumber - 24) - 16;
    }
    
    public int getCenter() {
        return this.width / 2;
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        if (button.id >= 0 && button.id < 5) {
            this.mc.displayGuiScreen((GuiScreen)new DuraColorMenuGui(this.mod, button.id + 1));
        }
        else {
            switch (button.id) {
                case 5: {
                    this.mod.openMainMenu();
                    break;
                }
                case 6: {
                    this.sm.getHelmetSettings().setShown(!this.sm.getHelmetSettings().isShown());
                    button.displayString = this.getModuleEnabledState(1);
                    break;
                }
                case 7: {
                    this.sm.getChestplateSettings().setShown(!this.sm.getChestplateSettings().isShown());
                    button.displayString = this.getModuleEnabledState(2);
                    break;
                }
                case 8: {
                    this.sm.getLeggingsSettings().setShown(!this.sm.getLeggingsSettings().isShown());
                    button.displayString = this.getModuleEnabledState(3);
                    break;
                }
                case 9: {
                    this.sm.getBootsSettings().setShown(!this.sm.getBootsSettings().isShown());
                    button.displayString = this.getModuleEnabledState(4);
                    break;
                }
                case 10: {
                    this.sm.getHelditemSettings().setShown(!this.sm.getHelditemSettings().isShown());
                    button.displayString = this.getModuleEnabledState(5);
                    break;
                }
            }
        }
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        super.drawDefaultBackground();
        this.mod.getHr().renderHud();
        final ItemStack helmet = new ItemStack(Item.getItemById(310));
        final ItemStack chestplate = new ItemStack(Item.getItemById(311));
        final ItemStack leggings = new ItemStack(Item.getItemById(312));
        final ItemStack boots = new ItemStack(Item.getItemById(313));
        final ItemStack held = new ItemStack(Item.getItemById(276));
        this.mod.getHr().drawItem(helmet, this.getCenter() - 8, this.getRowPos(2) - 12);
        this.mod.getHr().drawItem(chestplate, this.getCenter() - 8, this.getRowPos(3) - 14);
        this.mod.getHr().drawItem(leggings, this.getCenter() - 8, this.getRowPos(4) - 14);
        this.mod.getHr().drawItem(boots, this.getCenter() - 8, this.getRowPos(5) - 12);
        this.mod.getHr().drawItem(held, this.getCenter() - 8, this.getRowPos(6) - 8);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    
    private String getModuleEnabledState(final int module) {
        switch (module) {
            case 1: {
                return this.getStateLabel(this.sm.getHelmetSettings().isShown());
            }
            case 2: {
                return this.getStateLabel(this.sm.getChestplateSettings().isShown());
            }
            case 3: {
                return this.getStateLabel(this.sm.getLeggingsSettings().isShown());
            }
            case 4: {
                return this.getStateLabel(this.sm.getBootsSettings().isShown());
            }
            case 5: {
                return this.getStateLabel(this.sm.getHelditemSettings().isShown());
            }
            default: {
                return "";
            }
        }
    }
    
    private String getStateLabel(final boolean bool) {
        if (bool) {
            return "On";
        }
        return "Off";
    }
    
    public void onGuiClosed() {
        this.mod.getSm().saveSettings();
    }
}
