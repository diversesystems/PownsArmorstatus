package me.powns.armorhud.gui.menus;

import me.powns.armorhud.settings.modules.HeldItemSettings;
import me.powns.armorhud.settings.modules.BootsSettings;
import me.powns.armorhud.settings.modules.LeggingsSettings;
import me.powns.armorhud.settings.modules.ChestplateSettings;
import me.powns.armorhud.settings.modules.HelmetSettings;
import java.io.IOException;
import java.util.ArrayList;
import me.powns.armorhud.utils.GuiSlideControl;
import net.minecraft.client.gui.GuiButton;
import me.powns.armorhud.settings.GlobalSettings;
import me.powns.armorhud.settings.SettingsManager;
import me.powns.armorhud.ArmorHud;
import net.minecraft.client.gui.GuiScreen;

public class DuraColorMenuGui extends GuiScreen
{
    private ArmorHud mod;
    private SettingsManager sm;
    private GlobalSettings global;
    private int module;
    private int cat;
    private String[] modules;
    private String[] duras;
    private GuiButton nextCat;
    private GuiButton prevCat;
    private GuiButton chroma;
    private GuiButton goBack;
    private GuiSlideControl red;
    private GuiSlideControl green;
    private GuiSlideControl blue;
    private Object theSettings;
    
    public DuraColorMenuGui(final ArmorHud mod, final int module) {
        this.mod = mod;
        this.sm = this.mod.getSm();
        this.global = this.sm.getGlobalSettings();
        this.module = module;
        this.cat = 1;
        this.modules = new String[] { "Helmet", "Chestplate", "Leggings", "Boots", "Held Item" };
        this.duras = new String[] { "100% - 75%", "75% - 50%", "50% - 25%", "25% - 0%" };
        this.setTheSettings();
    }
    
    public void initGui() {
        this.setTheSettings();
        final ArrayList<Integer> rgb = this.getRGBVals();
        this.buttonList.add(this.chroma = new GuiButton(5, this.getCenter() - 75, this.getRowPos(3), 150, 20, "Chroma: " + this.getStateLabel(this.isChroma())));
        this.buttonList.add(this.nextCat = new GuiButton(0, this.getCenter() + 90, this.getRowPos(5), 20, 20, ">"));
        this.buttonList.add(this.prevCat = new GuiButton(1, this.getCenter() - 110, this.getRowPos(5), 20, 20, "<"));
        this.red = new GuiSlideControl(2, this.getCenter() - 75, this.getRowPos(4), 150, 20, "Red: ", 0.0f, 255.0f, rgb.get(0), true);
        this.green = new GuiSlideControl(3, this.getCenter() - 75, this.getRowPos(5), 150, 20, "Green: ", 0.0f, 255.0f, rgb.get(1), true);
        this.blue = new GuiSlideControl(4, this.getCenter() - 75, this.getRowPos(6), 150, 20, "Blue: ", 0.0f, 255.0f, rgb.get(2), true);
        this.buttonList.add(this.goBack = new GuiButton(6, this.getCenter() - 75, this.getRowPos(8), 150, 20, "Return"));
        this.buttonList.add(this.red);
        this.buttonList.add(this.green);
        this.buttonList.add(this.blue);
    }
    
    public int getRowPos(final int rowNumber) {
        return this.height / 4 + (24 * rowNumber - 24) - 16;
    }
    
    public int getCenter() {
        return this.width / 2;
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                if (this.cat + 1 > 4) {
                    this.cat = 1;
                }
                else {
                    ++this.cat;
                }
                this.mc.displayGuiScreen((GuiScreen)this);
                break;
            }
            case 1: {
                if (this.cat - 1 < 1) {
                    this.cat = 4;
                }
                else {
                    --this.cat;
                }
                this.mc.displayGuiScreen((GuiScreen)this);
                break;
            }
            case 5: {
                this.toggleChroma();
                button.displayString = "Chroma: " + this.getStateLabel(this.isChroma());
                break;
            }
            case 6: {
                this.mc.displayGuiScreen((GuiScreen)new ModulesMenuGui(this.mod));
                break;
            }
        }
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        super.drawDefaultBackground();
        final int duraPrevColor = this.getColor();
        this.drawCenteredString(this.mc.fontRendererObj, this.modules[this.module - 1] + " settings", this.getCenter(), this.getRowPos(1), -1);
        this.drawCenteredString(this.mc.fontRendererObj, "Durability: " + (this.isChroma() ? "Chroma" : this.duras[this.cat - 1]), this.getCenter(), this.getRowPos(2), duraPrevColor);
        this.updateColors();
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
    
    private String getStateLabel(final boolean bool) {
        if (bool) {
            return "Enabled";
        }
        return "Disabled";
    }
    
    private void toggleChroma() {
        switch (this.module) {
            case 1: {
                final HelmetSettings settings = (HelmetSettings)this.theSettings;
                settings.setChroma(!settings.isChroma());
                break;
            }
            case 2: {
                final ChestplateSettings settings2 = (ChestplateSettings)this.theSettings;
                settings2.setChroma(!settings2.isChroma());
                break;
            }
            case 3: {
                final LeggingsSettings settings3 = (LeggingsSettings)this.theSettings;
                settings3.setChroma(!settings3.isChroma());
                break;
            }
            case 4: {
                final BootsSettings settings4 = (BootsSettings)this.theSettings;
                settings4.setChroma(!settings4.isChroma());
                break;
            }
            case 5: {
                final HeldItemSettings settings5 = (HeldItemSettings)this.theSettings;
                settings5.setChroma(!settings5.isChroma());
                break;
            }
            default: {}
        }
    }
    
    private boolean isChroma() {
        switch (this.module) {
            case 1: {
                final HelmetSettings settings = (HelmetSettings)this.theSettings;
                return settings.isChroma();
            }
            case 2: {
                final ChestplateSettings settings2 = (ChestplateSettings)this.theSettings;
                return settings2.isChroma();
            }
            case 3: {
                final LeggingsSettings settings3 = (LeggingsSettings)this.theSettings;
                return settings3.isChroma();
            }
            case 4: {
                final BootsSettings settings4 = (BootsSettings)this.theSettings;
                return settings4.isChroma();
            }
            case 5: {
                final HeldItemSettings settings5 = (HeldItemSettings)this.theSettings;
                return settings5.isChroma();
            }
            default: {
                return false;
            }
        }
    }
    
    private void updateColors() {
        switch (this.module) {
            case 1: {
                final HelmetSettings settings = (HelmetSettings)this.theSettings;
                settings.setDuraColor(this.cat, this.red.GetValueAsInt(), this.green.GetValueAsInt(), this.blue.GetValueAsInt());
                break;
            }
            case 2: {
                final ChestplateSettings settings2 = (ChestplateSettings)this.theSettings;
                settings2.setDuraColor(this.cat, this.red.GetValueAsInt(), this.green.GetValueAsInt(), this.blue.GetValueAsInt());
                break;
            }
            case 3: {
                final LeggingsSettings settings3 = (LeggingsSettings)this.theSettings;
                settings3.setDuraColor(this.cat, this.red.GetValueAsInt(), this.green.GetValueAsInt(), this.blue.GetValueAsInt());
                break;
            }
            case 4: {
                final BootsSettings settings4 = (BootsSettings)this.theSettings;
                settings4.setDuraColor(this.cat, this.red.GetValueAsInt(), this.green.GetValueAsInt(), this.blue.GetValueAsInt());
                break;
            }
            case 5: {
                final HeldItemSettings settings5 = (HeldItemSettings)this.theSettings;
                settings5.setDuraColor(this.cat, this.red.GetValueAsInt(), this.green.GetValueAsInt(), this.blue.GetValueAsInt());
                break;
            }
            default: {}
        }
    }
    
    private void setTheSettings() {
        switch (this.module) {
            case 1: {
                this.theSettings = this.sm.getHelmetSettings();
                break;
            }
            case 2: {
                this.theSettings = this.sm.getChestplateSettings();
                break;
            }
            case 3: {
                this.theSettings = this.sm.getLeggingsSettings();
                break;
            }
            case 4: {
                this.theSettings = this.sm.getBootsSettings();
                break;
            }
            case 5: {
                this.theSettings = this.sm.getHelditemSettings();
                break;
            }
            default: {
                this.theSettings = null;
                break;
            }
        }
    }
    
    private int getColor() {
        switch (this.module) {
            case 1: {
                final HelmetSettings settings = (HelmetSettings)this.theSettings;
                return settings.getDuraColor(this.cat);
            }
            case 2: {
                final ChestplateSettings settings2 = (ChestplateSettings)this.theSettings;
                return settings2.getDuraColor(this.cat);
            }
            case 3: {
                final LeggingsSettings settings3 = (LeggingsSettings)this.theSettings;
                return settings3.getDuraColor(this.cat);
            }
            case 4: {
                final BootsSettings settings4 = (BootsSettings)this.theSettings;
                return settings4.getDuraColor(this.cat);
            }
            case 5: {
                final HeldItemSettings settings5 = (HeldItemSettings)this.theSettings;
                return settings5.getDuraColor(this.cat);
            }
            default: {
                return -1;
            }
        }
    }
    
    private ArrayList<Integer> getRGBVals() {
        switch (this.module) {
            case 1: {
                final HelmetSettings settings = (HelmetSettings)this.theSettings;
                return settings.getDuraRGBValues(this.cat);
            }
            case 2: {
                final ChestplateSettings settings2 = (ChestplateSettings)this.theSettings;
                return settings2.getDuraRGBValues(this.cat);
            }
            case 3: {
                final LeggingsSettings settings3 = (LeggingsSettings)this.theSettings;
                return settings3.getDuraRGBValues(this.cat);
            }
            case 4: {
                final BootsSettings settings4 = (BootsSettings)this.theSettings;
                return settings4.getDuraRGBValues(this.cat);
            }
            case 5: {
                final HeldItemSettings settings5 = (HeldItemSettings)this.theSettings;
                return settings5.getDuraRGBValues(this.cat);
            }
            default: {
                return null;
            }
        }
    }
    
    public void onGuiClosed() {
        this.mod.getSm().saveSettings();
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
}
