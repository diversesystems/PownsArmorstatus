package me.powns.armorhud.gui.menus;

import me.powns.armorhud.settings.modules.HeldItemSettings;
import me.powns.armorhud.settings.modules.BootsSettings;
import me.powns.armorhud.settings.modules.LeggingsSettings;
import me.powns.armorhud.settings.modules.ChestplateSettings;
import me.powns.armorhud.settings.modules.HelmetSettings;
import java.io.IOException;
import net.minecraft.client.gui.GuiButton;
import me.powns.armorhud.settings.GlobalSettings;
import me.powns.armorhud.settings.SettingsManager;
import me.powns.armorhud.ArmorHud;
import net.minecraft.client.gui.GuiScreen;

public class MainMenuGui extends GuiScreen
{
    private ArmorHud mod;
    private SettingsManager sm;
    private GlobalSettings global;
    private boolean dragging;
    private String focussedModule;
    private int lastMouseX;
    private int lastMouseY;
    private int lastAddX;
    private int lastAddY;
    private boolean regroupVertical;
    private GuiButton enable;
    private GuiButton durability;
    private GuiButton regroup;
    private GuiButton durabar;
    private GuiButton modules;
    
    public MainMenuGui(final ArmorHud mod) {
        this.mod = mod;
        this.sm = this.mod.getSm();
        this.global = this.sm.getGlobalSettings();
        this.focussedModule = "";
    }
    
    public void initGui() {
        this.buttonList.add(this.enable = new GuiButton(0, this.getCenter() - 75, this.getRowPos(3), 150, 20, this.getStateLabel(this.global.isEnabled())));
        this.buttonList.add(this.durability = new GuiButton(1, this.getCenter() - 75, this.getRowPos(4), 150, 20, "Durability: " + this.global.getCurrentDuraMode().getName()));
        this.buttonList.add(this.regroup = new GuiButton(2, this.getCenter() - 75, this.getRowPos(5), 150, 20, "Regroup"));
        this.buttonList.add(this.durabar = new GuiButton(3, this.getCenter() - 75, this.getRowPos(6), 150, 20, "Durability Bar: " + this.getStateLabel(this.global.isRenderOverlay())));
        this.buttonList.add(this.modules = new GuiButton(4, this.getCenter() - 75, this.getRowPos(7), 150, 20, "Edit Modules"));
    }
    
    public int getRowPos(final int rowNumber) {
        return this.height / 4 + (24 * rowNumber - 24) - 16;
    }
    
    public int getCenter() {
        return this.width / 2;
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        switch (button.id) {
            case 0: {
                this.global.setEnabled(!this.global.isEnabled());
                button.displayString = this.getStateLabel(this.global.isEnabled());
                break;
            }
            case 1: {
                if (this.global.getCurrentDuraModeIndex() + 1 > 3) {
                    this.global.setCurrentDuraMode(0);
                }
                else {
                    this.global.setCurrentDuraMode(this.global.getCurrentDuraModeIndex() + 1);
                }
                button.displayString = "Durability: " + this.global.getCurrentDuraMode().getName();
                break;
            }
            case 2: {
                this.regroupVertical = !this.regroupVertical;
                if (this.regroupVertical) {
                    this.mod.getHr().centerVertical();
                }
                else {
                    this.mod.getHr().centerHorizontal();
                }
                button.displayString = "Regroup: " + (this.regroupVertical ? "Vertical" : "Horizontal");
                break;
            }
            case 3: {
                this.global.setRenderOverlay(!this.global.isRenderOverlay());
                button.displayString = "Durability Bar: " + this.getStateLabel(this.global.isRenderOverlay());
                break;
            }
            case 4: {
                this.mc.displayGuiScreen((GuiScreen)new ModulesMenuGui(this.mod));
                break;
            }
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        if (state == 0) {
            this.dragging = false;
            this.focussedModule = "";
        }
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        super.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.mc.fontRendererObj.drawStringWithShadow("Powns ArmorStatus HUD", (float)(this.getCenter() - this.mc.fontRendererObj.getStringWidth("Powns ArmorStatus HUD") / 2), (float)this.getRowPos(2), -1);
        this.updateComponents(mouseX, mouseY);
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int clickedMouseButton, final long timeSinceLastClick) {
        final GlobalSettings global = this.mod.getSm().getGlobalSettings();
        final HelmetSettings helmet = this.mod.getSm().getHelmetSettings();
        final ChestplateSettings chestplate = this.mod.getSm().getChestplateSettings();
        final LeggingsSettings leggings = this.mod.getSm().getLeggingsSettings();
        final BootsSettings boots = this.mod.getSm().getBootsSettings();
        final HeldItemSettings helditem = this.mod.getSm().getHelditemSettings();
        final int test = helmet.getMaxX();
        if (!this.dragging && global.isEnabled()) {
            if (mouseX > helmet.getMinX() && mouseX < helmet.getMaxX() && mouseY > helmet.getMinY() && mouseY < helmet.getMaxY()) {
                this.dragging = true;
                this.lastMouseX = mouseX;
                this.lastMouseY = mouseY;
                this.lastAddX = helmet.getAddX();
                this.lastAddY = helmet.getAddY();
                this.focussedModule = "helmet";
                return;
            }
            if (mouseX > chestplate.getMinX() && mouseX < chestplate.getMaxX() && mouseY > chestplate.getMinY() && mouseY < chestplate.getMaxY()) {
                this.dragging = true;
                this.lastMouseX = mouseX;
                this.lastMouseY = mouseY;
                this.lastAddX = chestplate.getAddX();
                this.lastAddY = chestplate.getAddY();
                this.focussedModule = "chestplate";
                return;
            }
            if (mouseX > leggings.getMinX() && mouseX < leggings.getMaxX() && mouseY > leggings.getMinY() && mouseY < leggings.getMaxY()) {
                this.dragging = true;
                this.lastMouseX = mouseX;
                this.lastMouseY = mouseY;
                this.lastAddX = leggings.getAddX();
                this.lastAddY = leggings.getAddY();
                this.focussedModule = "leggings";
                return;
            }
            if (mouseX > boots.getMinX() && mouseX < boots.getMaxX() && mouseY > boots.getMinY() && mouseY < boots.getMaxY()) {
                this.dragging = true;
                this.lastMouseX = mouseX;
                this.lastMouseY = mouseY;
                this.lastAddX = boots.getAddX();
                this.lastAddY = boots.getAddY();
                this.focussedModule = "boots";
                return;
            }
            if (mouseX > helditem.getMinX() && mouseX < helditem.getMaxX() && mouseY > helditem.getMinY() && mouseY < helditem.getMaxY()) {
                this.dragging = true;
                this.lastMouseX = mouseX;
                this.lastMouseY = mouseY;
                this.lastAddX = helditem.getAddX();
                this.lastAddY = helditem.getAddY();
                this.focussedModule = "helditem";
            }
        }
    }
    
    private void updateComponents(final int mouseX, final int mouseY) {
        if (this.dragging) {
            final HelmetSettings helmet = this.mod.getSm().getHelmetSettings();
            final ChestplateSettings chestplate = this.mod.getSm().getChestplateSettings();
            final LeggingsSettings leggings = this.mod.getSm().getLeggingsSettings();
            final BootsSettings boots = this.mod.getSm().getBootsSettings();
            final HeldItemSettings helditem = this.mod.getSm().getHelditemSettings();
            if (this.focussedModule.equalsIgnoreCase("helmet")) {
                helmet.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
                helmet.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
                return;
            }
            if (this.focussedModule.equalsIgnoreCase("chestplate")) {
                chestplate.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
                chestplate.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
                return;
            }
            if (this.focussedModule.equalsIgnoreCase("leggings")) {
                leggings.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
                leggings.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
                return;
            }
            if (this.focussedModule.equalsIgnoreCase("boots")) {
                boots.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
                boots.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
                return;
            }
            if (this.focussedModule.equalsIgnoreCase("helditem")) {
                helditem.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
                helditem.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
            }
        }
    }
    
    private String getStateLabel(final boolean bool) {
        if (bool) {
            return "Enabled";
        }
        return "Disabled";
    }
    
    public void onGuiClosed() {
        this.mod.getSm().saveSettings();
    }
}
