package me.powns.armorhud.utils;

import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import java.text.DecimalFormat;
import net.minecraft.client.gui.GuiButton;

public class GuiSlideControl extends GuiButton
{
    public String label;
    public float curValue;
    public float minValue;
    public float maxValue;
    public boolean isSliding;
    public boolean useIntegers;
    private static DecimalFormat numFormat;
    
    public GuiSlideControl(final int id, final int x, final int y, final int width, final int height, final String displayString, final float minVal, final float maxVal, final float curVal, final boolean useInts) {
        super(id, x, y, width, height, useInts ? (displayString + (int)curVal) : (displayString + GuiSlideControl.numFormat.format((double)curVal)));
        this.label = displayString;
        this.minValue = minVal;
        this.maxValue = maxVal;
        this.curValue = (curVal - minVal) / (maxVal - minVal);
        this.useIntegers = useInts;
    }
    
    public float GetValueAsFloat() {
        return (this.maxValue - this.minValue) * this.curValue + this.minValue;
    }
    
    public int GetValueAsInt() {
        return (int)((this.maxValue - this.minValue) * this.curValue + this.minValue);
    }
    
    public String GetLabel() {
        if (this.useIntegers) {
            return this.label + this.GetValueAsInt();
        }
        return this.label + GuiSlideControl.numFormat.format((double)this.GetValueAsFloat());
    }
    
    protected void SetLabel() {
        this.displayString = this.GetLabel();
    }
    
    public int getHoverState(final boolean isMouseOver) {
        return 0;
    }
    
    protected void mouseDragged(final Minecraft mc, final int mousePosX, final int mousePosY) {
        if (this.visible) {
            if (this.isSliding) {
                this.curValue = (mousePosX - (this.xPosition + 4.0f)) / (this.width - 8.0f);
                if (this.curValue < 0.0f) {
                    this.curValue = 0.0f;
                }
                if (this.curValue > 1.0f) {
                    this.curValue = 1.0f;
                }
                this.SetLabel();
            }
            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.drawTexturedModalRect(this.xPosition + (int)(this.curValue * (this.width - 8)), this.yPosition, 0, 66, 4, 20);
            this.drawTexturedModalRect(this.xPosition + (int)(this.curValue * (this.width - 8)) + 4, this.yPosition, 196, 66, 4, 20);
        }
    }
    
    public boolean mousePressed(final Minecraft mc, final int mousePosX, final int mousePosY) {
        if (!super.mousePressed(mc, mousePosX, mousePosY)) {
            return false;
        }
        this.curValue = (mousePosX - (this.xPosition + 4.0f)) / (this.width - 8.0f);
        if (this.curValue < 0.0f) {
            this.curValue = 0.0f;
        }
        if (this.curValue > 1.0f) {
            this.curValue = 1.0f;
        }
        this.SetLabel();
        if (this.isSliding) {
            return this.isSliding = false;
        }
        return this.isSliding = true;
    }
    
    public void mouseReleased(final int mousePosX, final int mousePosY) {
        this.isSliding = false;
    }
    
    static {
        GuiSlideControl.numFormat = new DecimalFormat("#.00");
    }
}
