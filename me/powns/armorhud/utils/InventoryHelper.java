package me.powns.armorhud.utils;

import net.minecraft.item.ItemStack;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;

public class InventoryHelper
{
    public static int getItemStackCount(final Item is) {
        int count = 0;
        for (int i = 0; i < Minecraft.getMinecraft().thePlayer.inventory.getSizeInventory(); ++i) {
            if (Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(i) != null) {
                final ItemStack compareTo = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(i);
                final Item compareToItem = compareTo.getItem();
                final int compareToId = Item.getIdFromItem(compareToItem);
                final int filterItemId = Item.getIdFromItem(is);
                if (compareToId == filterItemId) {
                    count += compareTo.stackSize;
                }
            }
        }
        return count;
    }
}
