package me.powns.armorhud.command;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import me.powns.armorhud.ArmorHud;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;

public class ArmorHudCommand extends CommandBase
{
    private Minecraft mc;
    private ArmorHud mod;
    
    public ArmorHudCommand(final ArmorHud mod) {
        this.mc = Minecraft.getMinecraft();
        this.mod = mod;
    }
    
    public String getName() {
        return "armorhud";
    }

    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }

    @Override
    public String getCommandName() {
        return "armorhud";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/" + this.getName();
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        this.mod.openMainMenu();
    }
}
